export default function getParams(request: Request){

    const url = new URL(request.url);
    const sortfield = url.searchParams.getAll("sortfield");
    const sorttype = url.searchParams.getAll("sorttype");
    const pageSize = url.searchParams.getAll("pageSize");
    const page = url.searchParams.getAll("page");
    const filterfield = url.searchParams.getAll("filterfield");
    const filteroperator = url.searchParams.getAll("filteroperator");
    const filtervalue = url.searchParams.getAll("filtervalue");
    const searchVal = url.searchParams.getAll("search");

    const sortType = sorttype[0] || "asc";
    const sortField = sortfield[0] || "createdAt";
    const skip = parseInt(pageSize[0]) * (parseInt(page[0]) - 1) || 0;
    const take = parseInt(pageSize[0]) || 5;
    const pageNo = parseInt(page[0]) || 1;
    const field = filterfield[0] || "name";
    const operator = filteroperator[0] || "contains";
    let value = filtervalue[0] || "";
    const search = searchVal[0] || "";
    if (field === "price") {
      value = parseFloat(filtervalue[0]);
      console.log("valllll", value);
    } else {
      value = filtervalue[0] || "";
    }
    if (operator === "isEmpty") value = true;

    return {sortType, sortField, skip, take, pageNo, field, operator, value, search}
}