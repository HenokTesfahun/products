import getParams from "./getParams";
import { prisma } from "./prisma.server";

export default async function getAllProducts(request: Request) {
const {sortType, sortField, skip, take, pageNo, field, operator, value, search} = getParams(request)
const totalProduct = await prisma.product.count();
    const fetchedProduct = await prisma.product.count({
      where: {
        ...(search.length === 0
          ? {
              ...(field === "price"
                ? { [field]: { [operator]: value } }
                : { [field]: { [operator]: value, mode: "insensitive" } }),
            }
          : {
              OR: [
                {
                  name: {
                    contains: search,
                    mode: "insensitive",
                  },
                },
                {
                  description: {
                    contains: search,
                    mode: "insensitive",
                  },
                },
              ],
            }),
      },
    });

    const prod = await prisma.product.findMany({
      orderBy: [
        {
          [sortField]: sortType,
        },
      ],
      skip: skip,
      take: take,
      where: {
        ...(search.length === 0
          ? {
              ...(field === "price"
                ? { [field]: { [operator]: value } }
                : { [field]: { [operator]: value, mode: "insensitive" } }),
            }
          : {
              OR: [
                {
                  name: {
                    contains: search,
                    mode: "insensitive",
                  },
                },
                {
                  description: {
                    contains: search,
                    mode: "insensitive",
                  },
                },
              ],
            }),
      },
      select: {
        id: true,
        name: true,
        description: true,
        price: true,
        user: {
          select: {
            phonenumber: true,
          },
        },
        createdAt: true,
        updatedAt: true,
        status: true,
      },
    });
    const product = prod;
    product.map((item: any) => {
      const phonenumber = item.user.phonenumber;
      delete item.user;
      item.phonenumber = phonenumber;
      return item;
    });
    return {
      metaData: {
        sort: [sortField, sortType],
        pageSize: take,
        page: pageNo,
        totalProductCount: totalProduct,
        fetchedProductCount: fetchedProduct,
        searchVal: search,
      },
      data: product,
    }
}
