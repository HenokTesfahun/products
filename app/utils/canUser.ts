import { Permission } from "@prisma/client";
import { createAbility } from "./defineAbility";
import interpolate from "./interpolate";
import { prisma } from "./prisma.server";
import { ForbiddenError, subject } from '@casl/ability';
import type {AppAbility} from './defineAbility'

export default async function canUser(id: string, action: any, subj: any, data: any) {
    if(!data) data={}
    const user = await prisma.user.findUnique({
        where: { id },
        select: {
          id: true,
          status: true,
          role: {
            select: { 
                name: true,
                permissions: {
                select: {
                    permission: true
                }
            },
            },
          },
        },
      });
      if(user){
        let permissions = []
        
        user.role.permissions.map((item) => {
            if(item.permission.fields.length === 0)
            delete item?.permission?.fields
            permissions.push(item.permission)
        })
        user.role.permissions = interpolate(JSON.stringify(permissions), { user });
        const ability: AppAbility = createAbility(user.role.permissions);
        

        ForbiddenError.from(ability).throwUnlessCan(
          action,
          subject(subj, data)
        );
    
        return true
   }
}