import { useSearchParams, useSubmit } from "@remix-run/react";
import { useState } from "react";

export default function paramFunctions() {

    const [searchParams] = useSearchParams();
    const submit = useSubmit();
    const [focus, setFocus] = useState(false);
    const [val, setVal] = useState();

function getCurrentParams() {
    const param = searchParams?.toString();
    let paramObject = {};
    if (searchParams.keys().next().value) {
      paramObject = JSON.parse(
        '{"' + param.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
        function (key, value) {
          return key === "" ? value : decodeURIComponent(value);
        }
      );
      return paramObject;
    }
    return;
  }
  function handleSortModelChange(sortModel: any) {
    const paramObject = getCurrentParams();
    if (sortModel.length === 1) {
      const sort = sortModel[0];
      const sortField = sort.field;
      const sortType = sort.sort;
      submit({
        ...paramObject,
        sortfield: sortField,
        sorttype: sortType,
      });
    } else {
      submit({
        ...paramObject,
        sortfield: "createdAt",
        sorttype: "asc",
      });
    }
  }
  function onFilterChange(filterModel: any) {
    setFocus(false);
    const paramObject = getCurrentParams();
    if (paramObject?.search) {
      delete paramObject?.search;
    }
    const field = filterModel.items[0].columnField;
    let operator = filterModel.items[0].operatorValue;
    let value = filterModel.items[0].value;
    
    if (operator === "=") operator = "equals";
    else if (operator === ">") operator = "gt";
    else if (operator === "<") operator = "lt";
    else if (operator === ">=") operator = "gte";
    else if (operator === "<=") operator = "lte";
    if( paramObject?.filterfield && paramObject?.filterfield !== field){
      delete paramObject.filterfield
      delete paramObject.filteroperator
      delete paramObject.filtervalue
      submit({
        ...paramObject})
    }
    else if(operator === "isEmpty") {
      submit({
        ...paramObject,
        filterfield: field,
        filteroperator: operator,
        filtervalue: "true",
      });
    } else if (field && operator && value) {
      submit({
        ...paramObject,
        filterfield: field,
        filteroperator: operator,
        filtervalue: value,
      });
    } else {
      submit({
        ...paramObject,
        filterfield: "name",
        filteroperator: "contains",
        filtervalue: "",
      });
    }
  }
  function handlePageChange(newPage: any) {
    const paramObject = getCurrentParams();
    const page = newPage + 1;
    submit({
      ...paramObject,
      page: page,
    });
  }
  function handlePageSizeChange(newPageSize: any) {
    const paramObject = getCurrentParams();
    const pageSize = newPageSize;
    submit({
      ...paramObject,
      pageSize: pageSize,
    });
  }
  const handleSearch = (e) => {
      setFocus(true);
      setVal(e.target.value);
      const paramObject = getCurrentParams();
      if (paramObject?.filterfield) {
        delete paramObject?.filterfield;
        delete paramObject?.filteroperator;
        delete paramObject?.filtervalue;
      }
      submit({
        ...paramObject,
        search: e.target.value,
      });
    };
    return {handleSortModelChange, onFilterChange, handlePageChange, handlePageSizeChange, handleSearch, val, focus}
}
