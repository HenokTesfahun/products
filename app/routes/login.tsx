import {
  Box,
  Button,
  Grid,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import { ActionFunction, json, redirect } from "@remix-run/node";
import { Form, Link, useActionData } from "@remix-run/react";
import { useSnackbar } from "notistack";
import { prisma } from "~/utils/prisma.server";
import Write from "../assets/images/write.svg";

export const action: ActionFunction = async ({ request }) => {
  const form = await request.formData();
  const fields = Object.fromEntries(form);

  if (fields?.phonenumber === "") {
    return json({
      status: 400,
      data: {
        error: {
          message: "Phone Number is required",
        },
      },
    });
  }

  const phonenumber = "251" + fields.phonenumber;

  const user = await prisma.user.findUnique({
    where: { phonenumber },
  });

  if (user) {
    const otp = Math.floor(100000 + Math.random() * 900000);
    console.log(otp);
    const expire = new Date();
    expire.setTime(expire.getTime() + 3600000);

    const activeOtp = await prisma.otp.findFirst({
      where: { phonenumber, status: "ACTIVE" },
    });

    if (activeOtp) {
      await prisma.otp.update({
        where: {
          otp_phonenumber_status: {
            otp: activeOtp.otp,
            phonenumber,
            status: "ACTIVE",
          },
        },
        data: { status: "INACTIVE" },
      });
    }

    const verify = await prisma.otp.create({
      data: {
        otp: otp,
        phonenumber: user.phonenumber,
        expiresAt: expire,
      },
    });

    if (verify) {
      //Implement the code to send OTP to the User's Phone
      return redirect(`/${user.phonenumber}`);
    } else {
      return null;
    }
  } else {
    return json({
      status: 400,
      data: {
        error: {
          message: "Phone Number is not registered",
        },
      },
    });
  }
};

export default function Login() {
  const data = useActionData();
  const { enqueueSnackbar } = useSnackbar();
  if (data !== undefined) {
    if (data?.status === 400) {
      enqueueSnackbar(data?.data?.error?.message, {
        variant: "error",
        preventDuplicate: true,
      });
    }
  }
  return (
    <Box sx={{ maxHeight: "100vh", maxWidth: "100vw" }}>
      <Grid container sx={{ height: "calc(100vh - 64px)", width: "100%" }}>
        <Grid
          item
          md={7}
          sx={{ display: { xs: "none", sm: "none", md: "block" } }}
        >
          <Box
            sx={{
              bgcolor: "primary.main",
              height: "100%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h5"
                sx={{
                  color: "primary.main",
                  textShadow:
                    "-1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff",
                }}
              >
                PRODUCT APP
              </Typography>
            </Box>
            <Box sx={{ pt: 5 }}>
              <Box
                component="img"
                src={Write}
                sx={{ width: 450, height: 460 }}
              />
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={5}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              px: 10,
              height: "100%",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h4"
                sx={{
                  color: "white",
                  textShadow:
                    "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000",
                }}
              >
                LOGIN
              </Typography>
            </Box>
            <Box>
              <Form method="post">
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: 300,
                    pt: 12,
                  }}
                >
                  <TextField
                    name="phonenumber"
                    type="number"
                    label="Phone Number"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">+251</InputAdornment>
                      ),
                    }}
                  />

                  <Button variant="contained" type="submit" sx={{ mt: 5 }}>
                    LOGIN
                  </Button>
                </Box>
              </Form>
              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <Button>
                  <Link style={{ textDecoration: "none" }} to="/signup">
                    Don't have an account yet ?
                  </Link>
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}
