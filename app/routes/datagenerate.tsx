import type { LoaderFunction } from "@remix-run/node";
import { faker } from "@faker-js/faker";
import { prisma } from "~/utils/prisma.server";

export const loader: LoaderFunction = async ({ request }) => {
  const user = await prisma.user.findMany();
  if (user) {
    await user.map(async (item) => {
      const category = await prisma.category.create({
        data: {
          name: faker.commerce.department(),
          createdAt: faker.date.past(5),
        },
      });
      console.log("category", category);
      if (category) {
        Array.from({ length: 20 }).forEach(async () => {
          const product = await prisma.product.create({
            data: {
              name: faker.commerce.product(),
              description: faker.commerce.productDescription(),
              price: parseFloat(faker.commerce.price()),
              userId: item.id,
              createdAt: faker.date.past(3),
            },
          });
          await prisma.product.update({
            where: { id: product?.id },
            data: {
              categories: {
                create: {
                  category: {
                    connect: { id: category.id },
                  },
                },
              },
            },
          });
        });
      }
    });
  }
  return null;
};

export default function () {}
