import {
  Box,
  Button,
  Grid,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import { Form, Link, useActionData } from "@remix-run/react";
import Write from "../assets/images/write.svg";
import { z } from "zod";
import { ActionFunction, json, redirect } from "@remix-run/node";
import { prisma } from "~/utils/prisma.server";
import { useSnackbar } from "notistack";

export const schema = z.object({
  phonenumber: z
    .string()
    .min(1, { message: "Phone Number is required" })
    .regex(/^9\d{8}$/, { message: "Phone Number is Invalid" }),
});

export const action: ActionFunction = async ({ request }) => {
  const form = await request.formData();
  const fields = Object.fromEntries(form);

  const data = schema.safeParse(fields);
  if (!data.success) {
    const errorData = data?.error?.flatten();
    const validated = { ...errorData, fields };
    return json(validated);
  } else {
    const phonenumber = "251" + fields.phonenumber;
    const userExists = await prisma.user.findUnique({
      where: { phonenumber },
    });
    if (userExists) {
      return json({
        status: 400,
        data: {
          error: {
            message: "A user with this phone number already exists",
          },
        },
      });
    } else {
      const user = await prisma.user.create({
        data: {
          phonenumber,
          roleId: "cl9joxm3c0000c605hvttkth2",
        },
      });
      if (user) {
        const otp = Math.floor(100000 + Math.random() * 900000);
        console.log(otp);
        const expire = new Date();
        expire.setTime(expire.getTime() + 3600000);

        const verify = await prisma.otp.create({
          data: {
            otp: otp,
            phonenumber: user.phonenumber,
            expiresAt: expire,
          },
        });

        if (verify) {
          //Implement the code to send OTP to the User's Phone
          return redirect(`/${user.phonenumber}`);
        } else {
          return null;
        }
      } else {
        return null;
      }
    }
  }
};

export default function Signup() {
  const data = useActionData();
  const { enqueueSnackbar } = useSnackbar();
  if (data !== undefined) {
    if (data?.status === 400) {
      enqueueSnackbar(data?.data?.error?.message, {
        variant: "error",
        preventDuplicate: true,
      });
    }
  }
  return (
    <Box sx={{ maxHeight: "100vh", maxWidth: "100vw" }}>
      <Grid container sx={{ height: "calc(100vh - 64px)", width: "100%" }}>
        <Grid
          item
          md={7}
          sx={{ display: { xs: "none", sm: "none", md: "block" } }}
        >
          <Box
            sx={{
              bgcolor: "primary.main",
              height: "100%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h5"
                sx={{
                  color: "primary.main",
                  textShadow:
                    "-1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff",
                }}
              >
                PRODUCT APP
              </Typography>
            </Box>
            <Box sx={{ pt: 5 }}>
              <Box
                component="img"
                src={Write}
                sx={{ width: 450, height: 460 }}
              />
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={5}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              px: 10,
              height: "100%",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h4"
                sx={{
                  color: "white",
                  textShadow:
                    "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000",
                }}
              >
                SIGN UP
              </Typography>
            </Box>
            <Box>
              <Form method="post">
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: 300,
                    pt: 12,
                  }}
                >
                  <TextField
                    name="phonenumber"
                    type="number"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">+251</InputAdornment>
                      ),
                    }}
                    defaultValue={data?.fields?.phonenumber}
                    helperText={
                      data?.fieldErrors?.phonenumber
                        ? data?.fieldErrors?.phonenumber[0]
                        : ""
                    }
                    error={data?.fieldErrors?.phonenumber}
                  />

                  <Button variant="contained" type="submit" sx={{ mt: 5 }}>
                    Signup
                  </Button>
                </Box>
              </Form>
              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <Button>
                  <Link style={{ textDecoration: "none" }} to="/login">
                    Already have an account ?
                  </Link>
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}
