import { Box, Button, Grid, TextField, Typography } from "@mui/material";
import Write from "../assets/images/write.svg";
import { ActionFunction, json } from "@remix-run/node";
import { useActionData, Form, useSubmit } from "@remix-run/react";
import { useSnackbar } from "notistack";
import PinInput from "react-pin-input";
import { prisma } from "~/utils/prisma.server";
import { z } from "zod";
import { createUserSession } from "~/utils/session.server";

export const schema = z.object({
  otp: z
    .string()
    .min(1, { message: "Verification Code is required" })
    .regex(/\d{6}$/, {
      message: "Verification Code should only contain numbers",
    }),
});

export const action: ActionFunction = async ({ request, params }) => {
  const form = await request.formData();
  const fields = Object.fromEntries(form);
  const data = schema.safeParse(fields);
  if (!data.success) {
    const errorData = data?.error?.flatten();
    const validated = { ...errorData, fields };
    return json(validated);
  } else {
    const verify = await prisma.otp.findUnique({
      where: {
        otp_phonenumber_status: {
          otp: parseInt(fields?.otp),
          phonenumber: params?.verify,
          status: "ACTIVE",
        },
      },
    });

    if (verify) {
      const currentTime = new Date().getTime();
      const expires = verify?.expiresAt.getTime();
      if (currentTime > expires) {
        await prisma.otp.update({
          where: {
            otp_phonenumber_status: {
              otp: verify?.otp,
              phonenumber: verify.phonenumber,
              status: verify.status,
            },
          },
          data: {
            status: "INACTIVE",
          },
        });
        return json({
          status: 400,
          data: {
            error: {
              message: "Veification code has expired",
            },
          },
        });
      } else {
        await prisma.otp.update({
          where: {
            otp_phonenumber_status: {
              otp: verify?.otp,
              phonenumber: verify.phonenumber,
              status: verify.status,
            },
          },
          data: {
            status: "INACTIVE",
          },
        });
        const user = await prisma.user.update({
          where: { phonenumber: params.verify },
          data: { status: "ACTIVE" },
        });
        if (user) {
          return createUserSession(user.id, "/");
        }
      }
    } else {
      return json({
        status: 400,
        data: {
          error: {
            message: "Invalid Verification Code",
          },
        },
      });
    }
  }
};

export default function Verify() {
  const submit = useSubmit();
  const actionData = useActionData();
  const { enqueueSnackbar } = useSnackbar();
  if (actionData !== undefined) {
    if (actionData?.status !== 200) {
      enqueueSnackbar(actionData?.data?.error?.message, {
        variant: "error",
        preventDuplicate: true,
      });
    }
  }

  function handleComplete(value: any, index: any) {
    submit({ otp: value }, { method: "post" });
  }
  return (
    <Box sx={{ maxHeight: "100vh", maxWidth: "100vw" }}>
      <Grid container sx={{ height: "calc(100vh - 64px)", width: "100%" }}>
        <Grid
          item
          md={7}
          sx={{ display: { xs: "none", sm: "none", md: "block" } }}
        >
          <Box
            sx={{
              bgcolor: "primary.main",
              height: "100%",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h5"
                sx={{
                  color: "primary.main",
                  textShadow:
                    "-1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff",
                }}
              >
                PRODUCT APP
              </Typography>
            </Box>
            <Box sx={{ pt: 5 }}>
              <Box
                component="img"
                src={Write}
                sx={{ width: 450, height: 460 }}
              />
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={5}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              px: 10,
              height: "100%",
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Typography
                variant="h4"
                sx={{
                  color: "white",
                  textShadow:
                    "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000",
                }}
              >
                Enter
              </Typography>
              <Typography
                variant="h4"
                sx={{
                  color: "white",
                  textShadow:
                    "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000",
                }}
              >
                Verification
              </Typography>
              <Typography
                variant="h4"
                sx={{
                  color: "white",
                  textShadow:
                    "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000",
                }}
              >
                Code
              </Typography>
            </Box>
            <Box>
              <Form method="post">
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    width: 400,
                    pt: 12,
                  }}
                >
                  <Box>
                    <PinInput
                      length={6}
                      type="numeric"
                      inputMode="number"
                      style={{ padding: 15 }}
                      inputStyle={{
                        borderColor: "gray",
                        borderRadius: 15,
                        width: 40,
                        marginRight: 5,
                      }}
                      inputFocusStyle={{ borderColor: "green" }}
                      onComplete={(value, index) => {
                        handleComplete(value, index);
                      }}
                      autoSelect={true}
                      regexCriteria={/^[ A-Za-z0-9_@./#&+-]*$/}
                    />
                  </Box>

                  <Button
                    variant="contained"
                    type="submit"
                    fullWidth
                    disabled
                    sx={{ mt: 5 }}
                  >
                    Verify
                  </Button>
                  <Button>Resend Verification code</Button>
                </Box>
              </Form>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}
