import { Box } from "@mui/material";
import { getUserId } from "~/utils/session.server";
import { json, LoaderFunction } from "@remix-run/node";
import { DataGrid } from "@mui/x-data-grid";
import { Card } from "@mui/material";
import { useLoaderData } from "@remix-run/react";
import canUser from "~/utils/canUser";
import Slider from "~/src/Slider";
import getAllProducts from "~/utils/getAllProducts";
import paramFunctions from "~/utils/paramFunctions";
import CustomToolbar from "~/src/customToolbar";

export const loader: LoaderFunction = async ({ request }) => {
  const userId = await getUserId(request);
  if (!userId) {
    return null;
  }

  const can = await canUser(userId, "read", "Product", null);

  if (can) {
    const data = await getAllProducts(request);
    return json(data);
  } else {
    return {
      data: {
        error: {
          status: 403,
          message: "Unauthorized",
        },
      },
    };
  }
};

export default function products() {
  const data = useLoaderData();
  const {
    handleSortModelChange,
    onFilterChange,
    handlePageChange,
    handlePageSizeChange,
  } = paramFunctions();

  const columns = [
    { field: "name", headerName: "Name", width: 150 },
    { field: "description", headerName: "Description", width: 350 },
    { field: "price", headerName: "Price", width: 150, type: "number" },
    { field: "phonenumber", headerName: "Phone Number", width: 150 },
    { field: "createdAt", headerName: "Created At", width: 150, type: "date" },
    { field: "updatedAt", headerName: "Updated At", width: 150, type: "date" },
    { field: "status", headerName: "Status", width: 150 },
  ];

  if (data) {
    return (
      <Box sx={{ display: "flex" }}>
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            bgcolor: "secondary.main",
            p: 3,
            height: "calc(100vh - 64px)",
          }}
        >
          <Card>
            <Box
              sx={{
                height: "80vh",
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <DataGrid
                columns={columns}
                rows={data?.data}
                rowCount={data?.metaData?.fetchedProductCount}
                rowsPerPageOptions={[5, 10, 20, 30, 50, 75, 100]}
                page={data?.metaData?.page - 1}
                pageSize={data?.metaData?.pageSize}
                paginationMode="server"
                onPageChange={(newPage) => handlePageChange(newPage)}
                onPageSizeChange={(newPageSize) =>
                  handlePageSizeChange(newPageSize)
                }
                sortingMode="server"
                onSortModelChange={handleSortModelChange}
                filterMode="server"
                onFilterModelChange={onFilterChange}
                components={{ Toolbar: CustomToolbar }}
              />
            </Box>
          </Card>
        </Box>
      </Box>
    );
  } else {
    return <Slider />;
  }
}

// export function ErrorBoundary() {
//   return <Box>Unauthorized</Box>;
// }
