import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import { Link } from "@remix-run/react";
import { Avatar } from "@mui/material";
import Popover from "@mui/material/Popover";
import { useState } from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

export default function Layout({ children, userData }: any) {
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <Box>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Products
          </Typography>
          {userData ? (
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <Typography sx={{ mx: 2 }}>{userData.phonenumber}</Typography>
              <Avatar
                onClick={(event) => setAnchorEl(event.currentTarget)}
              ></Avatar>
              <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
              >
                <Box>
                  <List>
                    <ListItem disablePadding>
                      <Link to="/" style={{ textDecoration: "none" }}>
                        <ListItemButton
                          aria-label="profile"
                          sx={{ color: "black" }}
                        >
                          <ListItemText primary="Profile" />
                        </ListItemButton>
                      </Link>
                    </ListItem>
                    <ListItem disablePadding>
                      <Link to="/logout" style={{ textDecoration: "none" }}>
                        <ListItemButton
                          sx={{ color: "black" }}
                          aria-label="logout"
                        >
                          <ListItemText primary="Logout" />
                        </ListItemButton>
                      </Link>
                    </ListItem>
                  </List>
                </Box>
              </Popover>
            </Box>
          ) : (
            <Box sx={{ display: "flex" }}>
              <Button color="inherit">
                <Link
                  style={{ textDecoration: "none", color: "inherit" }}
                  to="/login"
                >
                  Login
                </Link>
              </Button>
              <Button color="inherit">
                <Link
                  style={{ textDecoration: "none", color: "inherit" }}
                  to="/signup"
                >
                  Register
                </Link>
              </Button>
            </Box>
          )}
        </Toolbar>
      </AppBar>
      <Box>{children}</Box>
    </Box>
  );
}
