import { Box } from "@mui/material";
import { GridToolbarFilterButton } from "@mui/x-data-grid";
import { Button, TextField } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import InputAdornment from "@mui/material/InputAdornment";
import paramFunctions from "~/utils/paramFunctions";

export default function CustomToolbar() {
  const { handleSearch, val, focus } = paramFunctions();
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        m: 3,
        alignItems: "center",
      }}
    >
      <TextField
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
        value={val}
        autoFocus={focus}
        onChange={handleSearch}
        sx={{ width: 400, bgcolor: "secondary.main" }}
      />
      <Box>
        <Button variant="contained">Add Product</Button>
        <GridToolbarFilterButton />
      </Box>
    </Box>
  );
}
