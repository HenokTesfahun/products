import { Avatar, Box, Typography } from "@mui/material";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";

function Slider() {
  //   const vw = Math.max(
  //     document.documentElement.clientWidth || 0,
  //     window.innerWidth || 0
  //   );
  //   const slideLeft = () => {
  //     var slider = document.getElementById("imageslider");
  //     slider.scrollLeft = slider?.scrollLeft - vw;
  //   };

  //   const slideRight = () => {
  //     var slider = document.getElementById("imageslider");
  //     slider.scrollLeft = slider?.scrollLeft + vw;
  //   };
  return (
    <Box
      id="imageslider"
      sx={{
        height: "calc(100vh - 64px)",
        display: "flex",
        overflowX: "auto",
        "&::-webkit-scrollbar": {
          display: "none",
        },
        scrollBehavior: "auto",
      }}
    >
      <Box
        sx={{
          backgroundImage: `url('https://c4.wallpaperflare.com/wallpaper/850/74/373/business-buy-card-commercial-wallpaper-preview.jpg')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          minWidth: "100vw",
          height: "100%",
          display: "flex",
          alignItems: "center",
        }}
      >
        <Box sx={{ pl: 10, width: 500 }}>
          <Typography
            color="primary.main"
            variant="h4"
            sx={{ fontWeight: "bold" }}
          >
            Products
          </Typography>
          <Typography color="primary.main" sx={{ py: 2 }}>
            Buy one for the price of two get one free
          </Typography>
          <Typography color="yellow">Read more</Typography>
        </Box>
      </Box>
      <Box
        sx={{
          backgroundImage: `url('https://previews.123rf.com/images/dreamcreation01/dreamcreation011907/dreamcreation01190700012/126642420-online-shopping-e-commerce-concept-with-character-vector-illustration.jpg')`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          minWidth: "100vw",
          minHeight: "100%",
          display: "flex",
          alignItems: "center",
        }}
      >
        <Box sx={{ pl: 10, width: 500 }}>
          <Typography
            color="primary.main"
            variant="h4"
            sx={{ fontWeight: "bold" }}
          >
            Products
          </Typography>
          <Typography color="primary.main" sx={{ py: 2 }}>
            Buy one for the price of two get one free
          </Typography>
          <Typography color="yellow">Read more</Typography>
        </Box>
      </Box>
    </Box>
  );
}

export default Slider;
