-- CreateEnum
CREATE TYPE "Status" AS ENUM ('PENDING', 'ACTIVE', 'INACTIVE', 'DELETED');

-- CreateTable
CREATE TABLE "Permission" (
    "id" STRING NOT NULL,
    "action" STRING NOT NULL,
    "subject" STRING NOT NULL,
    "conditions" JSONB,
    "fields" STRING[] DEFAULT ARRAY[]::STRING[],
    "status" "Status" NOT NULL DEFAULT 'ACTIVE',
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" STRING NOT NULL,
    "description" STRING NOT NULL,

    CONSTRAINT "Permission_pkey" PRIMARY KEY ("id")
);
